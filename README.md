# OpenML dataset: internet_usage

https://www.openml.org/d/372

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Internet Usage Data
 
 Data Type
 
    multivariate
 
 Abstract
 
    This data contains general demographic information on internet users
    in 1997.
 
 Sources
 
     Original Owner
 
 [1]Graphics, Visualization, & Usability Center
 College of Computing
 Geogia Institute of Technology
 Atlanta, GA
 
     Donor
 
 [2]Dr Di Cook
 Department of Statistics
 Iowa State University
 
    Date Donated: June 30, 1999
 
 Data Characteristics
 
    This data comes from a survey conducted by the Graphics and
    Visualization Unit at Georgia Tech October 10 to November 16, 1997.
    The full details of the survey are available [3]here.
 
    The particular subset of the survey provided here is the "general
    demographics" of internet users. The data have been recoded as
    entirely numeric, with an index to the codes described in the "Coding"
    file.
 
    The full survey is available from the web site above, along with
    summaries, tables and graphs of their analyses. In addition there is
    information on other parts of the survey, including technology
    demographics and web commerce.
 
 Data Format
 
    The data is stored in an ASCII files with one observation per line.
    Spaces separate fields.
 
 Past Usage
 
    This data was used in the American Statistical Association Statistical
    Graphics and Computing Sections 1999 Data Exposition.
      _________________________________________________________________
 
 
     [4]The UCI KDD Archive
     [5]Information and Computer Science
     [6]University of California, Irvine
     Irvine, CA 92697-3425
 
    Last modified: June 30, 1999
 
 References
 
    1. http://www.gvu.gatech.edu/gvu/user_surveys/survey-1997-10/
    2. http://www.public.iastate.edu/~dicook/
    3. http://www.cc.gatech.edu/gvu/user_surveys/survey-1997-10/
    4. http://kdd.ics.uci.edu/
    5. http://www.ics.uci.edu/
    6. http://www.uci.edu/


 Information about the dataset
 CLASSTYPE: nominal
 CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/372) of an [OpenML dataset](https://www.openml.org/d/372). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/372/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/372/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/372/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

